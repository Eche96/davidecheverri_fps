﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Move : MonoBehaviour
{
    private Checker checker;
    private bool canMove = true;

    private void Awake()
    {
        checker = GetComponent<Checker>();
    }
    private void FixedUpdate()
    {
        int x = (int)Input.GetAxis("Horizontal")*1;
        int y = (int)Input.GetAxis("Vertical")*1;
        if (canMove)
        {
            transform.Translate(x, transform.position.y, y,Space.Self) ;
            checker.Check();
        }
    }

    public void CantMove(bool _canMove)
    {
        canMove = _canMove;
    }

    public bool getCanmove { get { return canMove; } }
}
